import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

export const tweetApi = createApi({
	reducerPath: 'tweetApi',
	baseQuery: fetchBaseQuery({ baseUrl: 'https://5usshoqjge.execute-api.eu-west-1.amazonaws.com/prod' }),
	tagTypes: ["tweets"],
	endpoints: (builder) => ({
		getTweets: builder.query<
			{ tweets: Tweet[], cursor?: string },
			{ pageSize?: number, cursor?: string }
			>({
			query: ({ pageSize, cursor }) => ({
				url: `tweets`,
				method: 'GET',
				params: {
					pageSize,
					cursor
				},
				credentials: 'include',
			}),
			providesTags: ["tweets"]
		}),
		postTweet: builder.mutation<
			Tweet,
			{ message: string }
			>({
			query: ({ message }) => ({
				url: `tweets`,
				method: 'POST',
				body: {
					message
				},
				credentials: 'include',
			}),
			invalidatesTags: ["tweets"]
		}),
		likeTweet: builder.mutation<
			null,
			{ timestamp: string }
			>({
			query: ({ timestamp }) => ({
				url: `tweets/_like`,
				method: 'POST',
				body: {
					timestamp
				},
				credentials: 'include',
			}),
			invalidatesTags: ["tweets"]
		}),
	}),
})

export const { useGetTweetsQuery, useLikeTweetMutation, usePostTweetMutation } = tweetApi;
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import * as React from "react";
import {FC, useState} from "react";
import {IconButton} from "@mui/material";
import FavoriteIcon from '@mui/icons-material/Favorite';
import {useSession} from "next-auth/react";
import {useLikeTweetMutation} from "../../api/tweet.api";



interface Props {
	tweet: Tweet
}

// The approach used in this component shows how to built a sign in and sign out
// component that works on pages which support both client and server side
// rendering, and avoids any flash incorrect content on initial page load.
const Tweet: FC<Props> = ({ tweet }) => {
	const [liked, setLiked] = useState(false);
	const [likeTweet, results] = useLikeTweetMutation();
	const { data: session } = useSession();

	const like = () => {
		if (liked) {
			return
		}

		likeTweet({ timestamp: tweet.timestamp });

		setLiked(true)
	}

	return (
		<TableRow sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
			<TableCell>{tweet.username}</TableCell>
			<TableCell>{tweet.message}</TableCell>
			<TableCell>{tweet.timestamp}</TableCell>
			<TableCell>
				{tweet.likes}
				{session &&
                    <IconButton color="primary" aria-label="upload picture" component="label" onClick={like}>
                        <FavoriteIcon color={!liked ? "primary" : "secondary"}/>
                    </IconButton>
				}
			</TableCell>
		</TableRow>
	)
}

export default Tweet;

import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import TableBody from "@mui/material/TableBody";
import TableContainer from "@mui/material/TableContainer";
import * as React from "react";
import Tweet from "./tweet";
import {useGetTweetsQuery} from "../../api/tweet.api";
import {useState} from "react";

// The approach used in this component shows how to built a sign in and sign out
// component that works on pages which support both client and server side
// rendering, and avoids any flash incorrect content on initial page load.
const Tweets = () => {
	const [ pageSize, setPageSize ] = useState<number>(10);
	const [ cursor, setCursor ] = useState<string>();
	const { data, isSuccess, isLoading} = useGetTweetsQuery({ pageSize, cursor });

	if (!isSuccess) {
		return <></>
	}
	if (isLoading) {
		return <></>
	}

	return (
		<TableContainer component={Paper}>
			<Table sx={{ minWidth: 650 }} aria-label="simple table">
				<TableHead>
					<TableRow>
						<TableCell>Username</TableCell>
						<TableCell>Messages</TableCell>
						<TableCell>Date</TableCell>
						<TableCell>Likes</TableCell>
					</TableRow>
				</TableHead>
				<TableBody>
					{data.tweets.map((tweet) => (
						<Tweet key={tweet.timestamp} tweet={tweet} />
					))}
				</TableBody>
			</Table>
		</TableContainer>
	)
}

export default Tweets;
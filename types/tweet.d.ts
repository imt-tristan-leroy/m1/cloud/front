interface Tweet {
	username: string;
	message: string;
	timestamp: string;
	likes: number;
}
import Layout from "../components/layout/Layout";
import Tweets from "../components/tweets/tweets";
import AddTweet from "../components/tweets/add-tweet";

export default function Home() {
  return (
      <Layout>
        <h1>Rainbow Tweeter</h1>
          <AddTweet/>
          <Tweets/>
      </Layout>
  )
}
